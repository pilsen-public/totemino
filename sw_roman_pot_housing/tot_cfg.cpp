/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * tot_cfg.h - configuration storage routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */

#include <ctype.h>
#include <EEPROM.h>

#include "Arduino.h"
#include "tot_cfg.h"
#include "totemino_1.h"


/* User function - Write current configuration to EEPROM */
void func_write(void)
{
  int c;
  uint8_t i, eebuf[EBLK_LEN];
  
  eebuf[0] = bf_disable;
  memcpy(eebuf + 1, bf_dac, 5);
  eebuf[6] = 0;
  eebuf[7] = 0;
  i = CRC8(eebuf, EBLK_LEN); 
  eebuf[EBLK_LEN - 1] = i;

  Serial.write("Configuration written: ");
  for (i = 0; i < EBLK_LEN; i++) {
    EEPROM.write(i, eebuf[i]);
    if (eebuf[i] < 0x10) Serial.write('0');
    Serial.print(eebuf[i], HEX);
  }
  Serial.write('\n');
  
  return;  
}


/* User function - Read configuration from EEPROM */
void func_read(void)
{
  int c;
  uint8_t i, j;
  uint8_t eebuf[EBLK_LEN];

  Serial.write("Configuration read: ");
  for (i = 0; i < EBLK_LEN; i++) {
    eebuf[i] = EEPROM.read(i);
    if (eebuf[i] < 0x10) Serial.write('0');
    Serial.print(eebuf[i], HEX);
  }
  Serial.write('\n');
  j = eebuf[EBLK_LEN - 1];
  eebuf[EBLK_LEN - 1] = 0;
  i = CRC8(eebuf, EBLK_LEN);

  if (i == j) {
    Serial.print(F("Data OK.\n"));
    bf_disable = eebuf[0];
    memcpy(bf_dac, eebuf + 1, 5);    
  } else {
    Serial.print(F("Configuration not written before or data are corrupted.\n"));
  }

  return;  
}


/* Simple CRC protection */
byte CRC8(const byte *data, byte len)
{
  byte crc = 0x00;
  while (len--) {
    byte extract = *data++;
    for (byte tempI = 8; tempI; tempI--) {
      byte sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if (sum) {
        crc ^= 0x8C;
      }
      extract >>= 1;
    }
  }
  return crc;
}

