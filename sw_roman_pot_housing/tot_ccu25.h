/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * tot_ccu25.h - CCU25 compatible I2C routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */
 
#ifndef _TOT_CCU25_H_
#define _TOT_CCU25_H_

/* I2C comunication delay */
#define I2C_DELAY     100
/* Base address of the Mezzanine I2C mux PCA9543 */
#define I2C_ADR_MUX  0x70
/* I2C address of the I/O port PCF8574 */
#define I2C_ADR_PIO  0x20
/* I2C address of the ADC MAX11602 */
#define I2C_ADR_ADC  0x6D
/* I2C address of the DAC MAX5382 */
#define I2C_ADR_DAC  0x30


/* I2C address of the main multiplexer */
extern uint8_t i2c_adr_mux;

/* Start the I2C interface */
extern void ccu_i2c_begin(void);
/* Send one byte message in CCU25 format to I2C inteface */
extern uint8_t ccu_i2c_write(uint8_t addr, uint8_t data);
/* Read one byte in CCU25 format from I2C inteface */
extern uint8_t ccu_i2c_read(uint8_t addr, uint8_t *data);

#endif

