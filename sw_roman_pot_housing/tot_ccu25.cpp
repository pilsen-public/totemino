/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * tot_ccu25.h - CCU25 compatible I2C routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */

#include <Wire.h>

#include "Arduino.h"
#include "totemino_1.h"
#include "tot_ccu25.h"


/* I2C address of the main multiplexer */
uint8_t i2c_adr_mux = I2C_ADR_MUX;


/* Start the I2C interface */
void ccu_i2c_begin(void)
{
  Wire.begin();
  return;
}


/* Send one byte message in CCU25 format to I2C inteface */
uint8_t ccu_i2c_write(uint8_t addr, uint8_t data)
{
  uint8_t ret;

  digitalWrite(LED_CI2C, HIGH);
  Wire.beginTransmission(addr);
  Wire.write(data);
  ret = Wire.endTransmission();
  digitalWrite(LED_CI2C, LOW);
  return(ret);
}


/* Read one byte in CCU25 format from I2C inteface */
uint8_t ccu_i2c_read(uint8_t addr, uint8_t *data)
{
  uint8_t ret;

  digitalWrite(LED_CI2C, HIGH);
  ret = Wire.requestFrom((int) addr, (int) 1);
  *data = Wire.read();
  digitalWrite(LED_CI2C, LOW);
  return(ret);
}
