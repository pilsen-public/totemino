/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * totemino_1.h - 1st interactive application, the main program module
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */

#ifndef _TOTEMINO_H_
#define _TOTEMINO_H_

/* Red LED output */
#define LED_CI2C 4

/* Hybrid switching word buffer */
extern uint8_t bf_disable;
/* DAC buffer */
extern uint8_t bf_dac[5];
/* DAC chip selects */
extern const uint8_t dac_cs[5];

#endif
