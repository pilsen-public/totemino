/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * tot_texts.h - Text comments, constant strings stored in FLASH
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */

#ifndef _TOT_TEXTS_H_
#define _TOT_TEXTS_H_

/* Text comments for voltages */
const char s_v0[] PROGMEM = "Voltage VS1, 2V.1";
const char s_v1[] PROGMEM = "Voltage VS2, 2V.2";
const char s_v2[] PROGMEM = "Voltage VS3, 2V.3";
const char s_v3[] PROGMEM = "Voltage vS4, 2V.4";
const char s_v4[] PROGMEM = "Voltage VS5, 5V.1 (hybrid no. 1, 2)";
const char s_v5[] PROGMEM = "Voltage VS6, 5V.2 (hybrid no. 3, 4)";
const char s_v6[] PROGMEM = "Voltage VS7, 3V";
const char s_v7[] PROGMEM = "Testing value 5V";

const char* const txt_volt[] PROGMEM = {s_v0, s_v1, s_v2, s_v3, s_v4, s_v5, s_v6, s_v7};

/* Text comments for currents */
const char s_i0[] PROGMEM = "Current IS5, 5V.1 (hybrid no. 1, 2)";
const char s_i1[] PROGMEM = "Current IS6, 5V.2 (hybrid no. 3, 4)";
const char s_i2[] PROGMEM = "Current IS7, 3V";
const char s_i3[] PROGMEM = "Current IS1, 2V.1";
const char s_i4[] PROGMEM = "Current IS2, 2V.2";
const char s_i5[] PROGMEM = "Current IS3, 2V.3";
const char s_i6[] PROGMEM = "Current IS4, 2V.4";
const char s_i7[] PROGMEM = "Testing value 128.205 mA";

const char* const txt_curr[] PROGMEM = {s_i0, s_i1, s_i2, s_i3, s_i4, s_i5, s_i6, s_i7};

/* Text comments for overload indicators */
const char s_o0[] PROGMEM = "Reg. IC2A, 2V.1";
const char s_o1[] PROGMEM = "Reg. IC2B, 2V.2";
const char s_o2[] PROGMEM = "Reg. IC2C, 2V.3";
const char s_o3[] PROGMEM = "Reg. IC2D, 2V.4";
const char s_o4[] PROGMEM = "Reg. IC3A, 5V.1 (hybrid no. 1, 2)";
const char s_o5[] PROGMEM = "Reg. IC3B, 5V.2 (hybrid no. 3, 4)";
const char s_o6[] PROGMEM = "Reg. IC4, 3V";

const char* const txt_ovld[] PROGMEM = {s_o0, s_o1, s_o2, s_o3, s_o4, s_o5, s_o6};

/* DAC names */
const char s_d0[] PROGMEM = "2V1";
const char s_d1[] PROGMEM = "2V2";
const char s_d2[] PROGMEM = "2V3";
const char s_d3[] PROGMEM = "2V4";
const char s_d4[] PROGMEM = " 3V";

const char* const txt_dac[] PROGMEM = {s_d0, s_d1, s_d2, s_d3, s_d4};


#endif

