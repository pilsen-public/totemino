/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * tot_cfg.h - configuration storage routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */

#ifndef _TOT_CFG_H_
#define _TOT_CFG_H_

/* EEPROM block length */
#define EBLK_LEN        8


/* User function - Write current configuration to EEPROM */
extern void func_write(void);
/* User function - Read configuration from EEPROM */
extern void func_read(void);
/* Simple CRC protection */
extern byte CRC8(const byte *data, byte len);


#endif

