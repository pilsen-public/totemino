/* TOTEM Roman Pot FeedThrough Testing Device, Arduino based
 * totemino_1 - 1st interactive application, the main program module
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2017-08 rlinhart@kae.zcu.cz
 */
  
#include <ctype.h>
#include <avr/pgmspace.h>


#include "Arduino.h"
#include "tot_texts.h"
#include "tot_ccu25.h"
#include "tot_cfg.h"
#include "totemino_1.h"


/* If defined, the I2C mux base address is automatically found*/
#define ADR_AUTODETECT
/* If defined, selected preset will be automatically loaded */
#define CFG_PRESET
/* If defined, DACs and hybrid disable switches are updated after any configuration reading */
#define ALL_UPDATE


/* State - Idle */
#define STATE_IDLE    0xFE
/* State - Waiting for commands */
#define STATE_WAITING 0xFF
/* State - reading the Voltage ADC */
#define STATE_VADC    0x10
/* State - reading the Current ADC */
#define STATE_IADC    0x20
/* State - overload indicator read */
#define STATE_OVLD    0x30
/* State - hybrid disable setting */
#define STATE_DISB    0x40
/* State - DAC setting */
#define STATE_DACS    0x50


/* Universal string buffer */
char s[40];
/* Hybrid switching word buffer */
uint8_t bf_disable = 0xFF;
/* DAC buffer */
uint8_t bf_dac[5] = {0x7F, 0x7F, 0x7F, 0x7F, 0x7F};
/* DAC chip selects */
const uint8_t dac_cs[5] = {0xCF, 0xDF, 0x3F, 0x7F, 0xBF};


/* User function - Reading the voltage ADC */
uint8_t func_vadc(uint8_t state);
/* User function - Reading the current ADC */
uint8_t func_iadc(uint8_t state);
/* User function - Overload indicator reading */
uint8_t func_ovld(uint8_t state);
/* User function - Hybrid disable setting */
uint8_t func_disb(uint8_t state);
/* User function - DAC setting */
uint8_t func_dacs(uint8_t state);
/* User function - list the current configuration */
uint8_t func_list(void);
/* User function - Idle state */
void func_idle(void);
/* Update hzbrid disable and DACs */
void hd_dac_update(void);


/* Setup function */
void setup() {
  /* pin mode setting */
  pinMode(LED_CI2C, OUTPUT);
  digitalWrite(LED_CI2C, LOW);
  /* serial port initialization */
  Serial.begin(19200);
  Serial.setTimeout(10000);
  Serial.write('\n');
  /* I2C initilalization */
  ccu_i2c_begin();
  /* Load default preset */
#ifdef CFG_PRESET
  func_read();
#endif
#ifdef ALL_UPDATE
  hd_dac_update();
#endif
  return;
}


/* Main loop function */
void loop() {
  static uint8_t state = 0;
  uint8_t chan, a;
  float rsen, f;
  int c;

/* Autodetect the I2C address */
#ifdef ADR_AUTODETECT
  if (state < 4) {
    Serial.write("Looking for mezzanine mux at 0x");
    Serial.print(i2c_adr_mux, HEX);
    Serial.write(" ... ");
    if (ccu_i2c_write(i2c_adr_mux, 0)) {
      Serial.write("no response.\n");
      i2c_adr_mux++;
      state++;
      if (state == 4) {
        state = 0;
        i2c_adr_mux = I2C_ADR_MUX;
      }
    } else {
      Serial.write("found one.\n");
      state = STATE_IDLE;
    }
    delay(500);
    return;
  }
#else
  if (state < 4) {
    state = STATE_IDLE;
  }
#endif

  /* Reading the Voltage ADCs */
  if ((state & 0xF0) == STATE_VADC) {  
    if (func_vadc(state & 0x0F)) state++;
      else state = STATE_IDLE;
  }

  /* Reading the Current ADCs */
  if ((state & 0xF0) == STATE_IADC) {  
    if (func_iadc(state & 0x0F)) state++;
      else state = STATE_IDLE;
  }

  /* Overload indicator reading */
  if ((state & 0xF0) == STATE_OVLD) {  
    if (func_ovld(state & 0x0F)) state++;
      else state = STATE_IDLE;
  }

  /* Hybrid disable setting */
  if ((state & 0xF0) == STATE_DISB) {  
    if (func_disb(state & 0x0F)) state++;
      else state = STATE_IDLE;
  }

  /* DAC settings */
  if ((state & 0xF0) == STATE_DACS) {
    if (func_dacs(state & 0x0F)) state++;
      else state = STATE_IDLE;
  }
    
  /* Idle state, display command prompt and mux disable */
  if (state == STATE_IDLE) {
    func_idle();
    state = STATE_WAITING;
  }
      
  /* Next function selection */
  if (state == STATE_WAITING) {
    c = Serial.read();
    switch (c) {
      case -1:
        break;
      case 'v':
        state = STATE_VADC;
        break;
      case 'c':
        state = STATE_IADC;
        break;
      case 'o':
        state = STATE_OVLD;
        break;
      case 'd':
        state = STATE_DISB;
        break;
      case 's':
        state = STATE_DACS;
        break;
      case 'w':
        func_write();
        state = STATE_IDLE;
        break;
      case 'r':
        func_read();
#ifdef ALL_UPDATE
        hd_dac_update();
#endif
        state = STATE_IDLE;
        break;
      case 'l':
        func_list();
        state = STATE_IDLE;
        break;
      default:
        if (c != '\n' && c != '\r') state = STATE_IDLE;
    }
  }

  return;
}


/* User function - Reading the voltage ADC */
uint8_t func_vadc(uint8_t state)
{
  uint8_t chan, a;
  float f;

  /* Set the main multiplexer */
  if (state == 0) {
    ccu_i2c_write(i2c_adr_mux, 0x01);
    delay(I2C_DELAY);
    ccu_i2c_write(i2c_adr_mux, 0x01);
    return(1);
  }

  /* Set the ADC and start conversion */
  if (state == 1) {
    ccu_i2c_write(I2C_ADR_ADC, 0xFA);
    return(1);
  }

  /* Read the result */
  if (state >= 2 && state < 10) {
    chan = state - 2;
    ccu_i2c_write(I2C_ADR_ADC, 0x61 | (chan << 1));
    ccu_i2c_read(I2C_ADR_ADC, &a);
    ccu_i2c_read(I2C_ADR_ADC, &a);
    Serial.write("Voltage chan. ");
    Serial.print(chan, DEC);
    Serial.write(": 0x");
    if (a < 0x10) Serial.write('0');
    Serial.print(a, HEX);
    Serial.write(" ~ ");
    f = float (a) / 255.0F * 4.096F * 2.0F;
    Serial.print(f, 3);
    Serial.write(" V ... ");
    strcpy_P(s, (char*) pgm_read_word(&(txt_volt[chan])));
    Serial.write(s);
    Serial.write('\n');
    if (state >= 10) return(0); else return(1);
  }

  return(0);
}

/* User function - Reading the current ADC */
uint8_t func_iadc(uint8_t state)
{
  uint8_t chan, a;
  float rsen, f;

  /* Set the main multiplexer */
  if (state == 0) {
    ccu_i2c_write(i2c_adr_mux, 0x02);
    delay(I2C_DELAY);
    ccu_i2c_write(i2c_adr_mux, 0x02);
    return(1);
  }

  /* Set the ADC and start conversion */
  if (state == 1) {
    ccu_i2c_write(I2C_ADR_ADC, 0xFA);
    return(1);
  }

  /* Read the result */
  if (state >= 2 && state < 10) {
    chan = state - 2;
    ccu_i2c_write(I2C_ADR_ADC, 0x61 | (chan << 1));
    ccu_i2c_read(I2C_ADR_ADC, &a);
    ccu_i2c_read(I2C_ADR_ADC, &a);
    Serial.write("Current chan. ");
    Serial.print(chan, DEC);
    Serial.write(": 0x");
    if (a < 0x10) Serial.write('0');
    Serial.print(a, HEX);
    Serial.write(" ~ ");
    f = float (a) / 255.0F * 4.096F;
    rsen = (chan < 3) ? 0.039F : 0.39F;
    f = f / rsen * 20.0F;
    Serial.print(f, 3);
    Serial.write(" mA ... ");
    strcpy_P(s, (char*) pgm_read_word(&(txt_curr[chan])));
    Serial.write(s);
    Serial.write('\n');
    if (state >= 10) return(0); else return(1);
  }

  return(0);
}


/* User function - Overload indicator reading */
uint8_t func_ovld(uint8_t state)
{
  uint8_t n, a, c;

  /* Set the main multiplexer */
  if (state == 0) {
    ccu_i2c_write(i2c_adr_mux, 0x01);
    delay(I2C_DELAY);
    ccu_i2c_write(i2c_adr_mux, 0x01);
    return(1);
  }

  /* Preset the PIO */
  if (state == 1) {
    ccu_i2c_write(I2C_ADR_PIO, 0xFF);
    delay(I2C_DELAY);
    return(1);
  }

  /* Read the state */
  if (state == 2) {
    ccu_i2c_read(I2C_ADR_PIO, &a);
    Serial.write("Overload indicator word 0x");
    if (a < 0x10) Serial.write('0');
    Serial.print(a, HEX);
    Serial.write(" means:\n");
    for (c = 0, n = 1; c < 7; c++, n <<= 1) {
      Serial.print(c, DEC);
      Serial.write(": ");
      Serial.write((a & n) ? '1' : '0');
      Serial.write(" - ");
      if (a & n) Serial.write("OK"); else Serial.write("OVERLOAD");
      Serial.write(" ... ");
      strcpy_P(s, (char*) pgm_read_word(&(txt_ovld[c])));
      Serial.write(s);
      Serial.write('\n');
    }
    state = STATE_IDLE;
    return(0);
  }
  
  return(0);
}


/* User function - Hybrid disable setting */
uint8_t func_disb(uint8_t state)
{
  uint8_t n, a;
  int c;

  /* Prepare the switching word changes */
  if (state == 0) {
    Serial.write("Hybrid? (1-4)\n");
    do {
      c = Serial.read();
      if (isalnum(c)) {
        Serial.write(c);
        Serial.write('\n');
      }
    } while (c != '1' && c !='2' && c !='3' && c !='4');
    a = 1 << (c - '1');

    Serial.write("(d)isable or (e)nable?\n");
    do {
      c = Serial.read();
      if (isalnum(c)) {
        Serial.write(c);
        Serial.write('\n');
      }
    } while (c != 'd' && c !='e');

    if (c == 'e') bf_disable |= a;
      else bf_disable &= ~a;

    Serial.write("New switch state is 0x");
    if (bf_disable < 0x10) Serial.write('0');
    Serial.print(bf_disable, HEX);
    Serial.write(" which means:\n");
    for (c = 0, n = 1; c < 4; c++, n <<= 1) {
      Serial.write("Hybrid ");
      Serial.print(c + 1, DEC);
      Serial.write(" is ");
      if (bf_disable & n) Serial.write("ON"); else Serial.write("OFF");
      Serial.write('\n');
    }

    ccu_i2c_write(i2c_adr_mux, 0x02);
    delay(I2C_DELAY);
    ccu_i2c_write(i2c_adr_mux, 0x02);
    return(1);
  }

  /* Set the switching word */
  if (state == 1) {
    ccu_i2c_write(I2C_ADR_PIO, bf_disable | 0xF0);
    return(0);
  }
  
  return(0);
}


/* User function - DAC setting */
uint8_t func_dacs(uint8_t state)
{
  int c;
  static uint8_t chan;

  /* Prepare the buffer change */
  if (state == 0) {
    Serial.print(F("Which DAC? (1~Pre1, 2~Pre2, 3~Pre3, 4~Pre4, 5~Booster)\n"));
    do {
      c = Serial.read();
      if (isalnum(c)) {
        Serial.write(c);
        Serial.write('\n');
      }
    } while (c != '1' && c !='2' && c !='3' && c !='4' && c !='5');
    chan = c - '1';

    Serial.write("Previous value was ");
    Serial.print(bf_dac[chan], DEC);
    Serial.write(", new one? (0-255)\n");
    Serial.read();
    while (!Serial.available())
      ;
    do {
      c = Serial.parseInt();
      Serial.print(c, DEC);
      Serial.write('\n');
    } while (c < 0 || c > 255);
    bf_dac[chan] = c;

    ccu_i2c_write(i2c_adr_mux, 0x02);
    delay(I2C_DELAY);
    ccu_i2c_write(i2c_adr_mux, 0x02);
    return(1);
  }

  /* Set the DAC */
  if (state == 1) {
    ccu_i2c_write(I2C_ADR_PIO, (bf_disable | 0xF0) & dac_cs[chan]);
    delay(I2C_DELAY);
    ccu_i2c_write(I2C_ADR_DAC, bf_dac[chan]);
    delay(I2C_DELAY);
    ccu_i2c_write(I2C_ADR_PIO, bf_disable | 0xF0);
    Serial.write("DAC no. ");
    Serial.print(chan + 1, DEC);
    Serial.write(' ');
    Serial.write('(');
    strcpy_P(s, (char*) pgm_read_word(&(txt_dac[chan])));
    Serial.write(s);
    Serial.write(')');
    Serial.write(" was set to ");
    Serial.print(bf_dac[chan], DEC);
    Serial.write(".\n");
    return(0);
  }

  return(0);
}


/* User function - list the current configuration */
uint8_t func_list(void)
{
  uint8_t i, n;
  
  Serial.write("Hybrid switch state is 0x");
  if (bf_disable < 0x10) Serial.write('0');
  Serial.print(bf_disable, HEX);
  Serial.write(" which means:\n");
  for (i = 0, n = 1; i < 4; i++, n <<= 1) {
    Serial.write("Hybrid ");
    Serial.print(i + 1, DEC);
    Serial.write(" is ");
    if (bf_disable & n) Serial.write("ON"); else Serial.write("OFF");
      Serial.write('\n');
  }
  for (i = 0; i < 5; i++) {
    Serial.write("DAC no. ");
    Serial.print(i + 1, DEC);
    Serial.write(' ');
    Serial.write('(');
    strcpy_P(s, (char*) pgm_read_word(&(txt_dac[i])));
    Serial.write(s);
    Serial.write(')');
    Serial.write(" was set to ");
    Serial.print(bf_dac[i], DEC);
    Serial.write('\n');
  }
  return;
}


/* User function - Idle state */
void func_idle(void)
{
  Serial.print(F("Commands: (v)oltages, (c)urrents, (o)verload, (d)isable, (s)et, "));
  Serial.print(F("(w)rite, (r)ead, (l)ist?\n"));
  ccu_i2c_write(i2c_adr_mux, 0x00);
  delay(I2C_DELAY);
  ccu_i2c_write(i2c_adr_mux, 0x00);
  delay(I2C_DELAY);
  return;
}


/* Update hzbrid disable and DACs */
void hd_dac_update(void)
{
  uint8_t i;
  
  ccu_i2c_write(i2c_adr_mux, 0x02);
  delay(I2C_DELAY);
  ccu_i2c_write(i2c_adr_mux, 0x02);
  delay(I2C_DELAY);
  for (i = 0; i < 5; i++) {
    ccu_i2c_write(I2C_ADR_PIO, (bf_disable | 0xF0) & dac_cs[i]);
    delay(I2C_DELAY);
    ccu_i2c_write(I2C_ADR_DAC, bf_dac[i]);
    delay(I2C_DELAY);
  }
  ccu_i2c_write(I2C_ADR_PIO, bf_disable | 0xF0);
  delay(I2C_DELAY);
  return;
}

