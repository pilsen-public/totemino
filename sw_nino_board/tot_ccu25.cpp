/* TOTEM Roman Pot FeedThrough and Nino Board Testing Device Arduino based
 * tot_ccu25.h - CCU25 compatible I2C routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@fel.zcu.cz
 */

#include <Wire.h>

#include "Arduino.h"
#include "tot_ccu25.h"


/* I2C comunication delay */
#define I2C_DELAY     100

/* Red LED output */
#define LED_CI2C 4


/* Start the I2C interface */
void ccu_i2c_init(void)
{
  // LED pin mode setting
  pinMode(LED_CI2C, OUTPUT);
  digitalWrite(LED_CI2C, LOW);
  // I2C init
  Wire.begin();
  delay(I2C_DELAY);
  return;
}


/* Send one byte message in CCU25 format to I2C inteface */
uint8_t ccu_i2c_write(uint8_t addr, uint8_t data)
{
  uint8_t ret;

  digitalWrite(LED_CI2C, HIGH);
  Wire.beginTransmission(addr);
  Wire.write(data);
  ret = Wire.endTransmission();
  digitalWrite(LED_CI2C, LOW);
  delay(I2C_DELAY);
  return(ret);
}


/* Read one byte in CCU25 format from I2C inteface */
uint8_t ccu_i2c_read(uint8_t addr, uint8_t *data)
{
  uint8_t ret;

  digitalWrite(LED_CI2C, HIGH);
  ret = Wire.requestFrom((int) addr, (int) 1);
  *data = Wire.read();
  digitalWrite(LED_CI2C, LOW);
  delay(I2C_DELAY);
  return(ret);
}
