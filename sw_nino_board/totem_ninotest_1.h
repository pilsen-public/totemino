/* TOTEM Nino Board Testing Device Arduino based
 * totem_ninotest_1.h - this file us used for global test configuration
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@kae.zcu.cz
 */

#ifndef _TOTEM_NINOTEST_H_
#define _TOTEM_NINOTEST_H_

/* If defined, the I2C mux base address is automatically found */
#define ADR_AUTO_MUX
/* If defined, the I2C DAC address is automatically found */
#define ADR_AUTO_DAC
/* If defined, selected configuration will be automatically set on tested system */
#define CFG_PRESET


/* Base 7bit I2C address of the Mezzanine I2C mux PCA9543 */
#define I2C_ADR_MUX_BASE  0x70
/* Base 7bit I2C address of the DAC MAX5382 */
#define I2C_ADR_DAC_BASE  0x30
/* Predefined offset from the base of the 7bit I2C address of the Mezzanine I2C mux PCA9543 */
#define I2C_ADR_MUX_OFFS     0
/* Predefined offset from the base of the 7bit I2C address of the DAC MAX5382 */
#define I2C_ADR_DAC_OFFS     0
/* Maximal offset from the base of the 7bit I2C address of the Mezzanine I2C mux PCA9543 */
#define I2C_ADR_MUX_MAX      3
/* Maximal offset from the base of the 7bit I2C address of the DAC MAX5382 */
#define I2C_ADR_DAC_MAX      3

/* DAC data buffer length */
#define DAC_COUNT            4
/* DAC default contents value setting */
#define DAC_DEFAULT       0x7F
/* Predefined DAC reference voltage i mV */
#define REF_DAC           2970
/* DAC no. 1 reference voltage in mV */
#define REF_DAC_1      REF_DAC
/* DAC no. 2 reference voltage in mV */
#define REF_DAC_2      REF_DAC
/* DAC no. 3 reference voltage in mV */
#define REF_DAC_3      REF_DAC
/* DAC no. 4 reference voltage in mV */
#define REF_DAC_4      REF_DAC

#endif
