/* TOTEM Roman Pot FeedThrough and Nino Board Testing Device Arduino based
 * tot_ccu25.h - CCU25 compatible I2C routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@fel.zcu.cz
 */
 
#ifndef _TOT_CCU25_H_
#define _TOT_CCU25_H_


/* Start the I2C interface */
extern void ccu_i2c_init(void);

/* Send one byte message in CCU25 format to I2C inteface */
extern uint8_t ccu_i2c_write(uint8_t addr, uint8_t data);

/* Read one byte in CCU25 format from I2C inteface */
extern uint8_t ccu_i2c_read(uint8_t addr, uint8_t *data);


#endif
