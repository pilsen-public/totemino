/* TOTEM Roman Pot FeedThrough and Nino Board Testing Device Arduino based
 * tot_cfg.h - configuration storage routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@fel.zcu.cz
 */

#include <ctype.h>
#include <EEPROM.h>

#include "Arduino.h"
#include "tot_cfg.h"
#include "totem_ninotest_1.h"


/* Eeprom write pointer */
static uint16_t ptre_wr = 0;
/* Eeprom read pointer */
static uint16_t ptre_rd = 0;


/* Simple CRC protection */
static byte CRC8(const byte *data, byte len);


/* User configuration - reset the read and write pointer */
void user_conf_reset(void)
{
  ptre_wr = 0; ptre_rd = 0;
  return;
}


/* User configuration - Write to the EEPROM */
uint8_t user_conf_write(uint8_t *buf, uint16_t len)
{
  uint16_t i;
  uint8_t *p, c;

  Serial.write("Conf write:\n");

  // save the block length
  Serial.print(ptre_wr, HEX);
  Serial.write(':');
  c = len >> 8;
  EEPROM.write(ptre_wr++, c);
  if (c < 16) Serial.print('0');
  Serial.print(c, HEX);
  c = len & 0xFF;
  EEPROM.write(ptre_wr++, c);
  if (c < 16) Serial.print('0');
  Serial.print(c, HEX);
  Serial.write(',');

  // save the block of data
  for (i = 0, p = buf; i < len; i++, p++)
  {
    EEPROM.write(i + ptre_wr, *p);
    if (*p < 16) Serial.print('0');
    Serial.print(*p, HEX);
  }
  ptre_wr += len;
  Serial.print(',');

  // save the CRC
  c = CRC8(buf, len);
  EEPROM.write(ptre_wr++, c);
  if (c < 16) Serial.print('0');
  Serial.print(c, HEX);
  Serial.write('\n');

  return(len);  
}


/* User configuration - Read from the EEPROM */
uint8_t user_conf_read(uint8_t *buf, uint16_t len)
{
  uint16_t i, slen;
  uint8_t c, *p;

  Serial.write("Conf read: ");

  // block length check
  slen = EEPROM.read(ptre_rd++);
  slen <<= 8;
  slen |= EEPROM.read(ptre_rd++);

  if (slen != len)
  {
    if (slen == 65535)
    {
      Serial.print("empty memory, no data found.");
      return(CONF_RD_FAILED);
    }
    Serial.print("ERROR! ");
    Serial.print(len, DEC);
    Serial.print(" bytes expected, but ");
    Serial.print(slen, DEC);
    Serial.print(" bytes block found. Nothing is read.\n");
    return(CONF_RD_FAILED);
  }
  
  Serial.print(len, DEC);
  Serial.print(" bytes\n");

  // read - destructive for the buffer contents!
  for (i = 0, p = buf; i < len; i++, p++)
  {
    *p = EEPROM.read(ptre_rd++);
    if (*p < 16) Serial.print('0');
    Serial.print(*p, HEX);
  }
  Serial.print(',');

  // CRC check
  c = EEPROM.read(ptre_rd++);
  if (c < 16) Serial.print('0');
  Serial.print(c, HEX);
  if (c != CRC8(buf, len))
  {
    Serial.print(" CRC ERROR!\n");
    return(CONF_RD_FAILED);
  } else {
    Serial.print(" CRC OK\n");
  }

  return(CONF_RD_OK);
}


/* Simple CRC protection */
static byte CRC8(const byte *data, byte len)
{
  byte crc = 0x00;
  while (len--)
  {
    byte extract = *data++;
    for (byte tempI = 8; tempI; tempI--)
    {
      byte sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if (sum)
      {
        crc ^= 0x8C;
      }
      extract >>= 1;
    }
  }
  return crc;
}
