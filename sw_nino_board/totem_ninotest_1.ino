/* TOTEM Nino Board Testing Device Arduino based
 * totem_ninotest_1.ino - the main program module
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@fel.zcu.cz
 */
  
#include <ctype.h>
#include <avr/pgmspace.h>


#include "Arduino.h"
#include "totem_ninotest_1.h"
#include "tot_ccu25.h"
#include "tot_cfg.h"


/* The Top Level state typedef */
typedef enum
{
  S_LOAD_CFG,
  S_DETECT_MUX,
  S_DETECT_DAC,
  S_DAC_UPDATE,
  S_WAIT_CMD,
  S_SET,
  S_WRITE_CFG,
  S_READ_CFG,
  S_LIST
} state_t;


/* A 7bit I2C address of the main I2C multiplexer */
uint8_t i2c_adr_mux = I2C_ADR_MUX_BASE + I2C_ADR_MUX_OFFS;
/* A 7bit I2C address of the DAC */
uint8_t i2c_adr_dac[DAC_COUNT] =
  {I2C_ADR_DAC_BASE + I2C_ADR_DAC_OFFS, I2C_ADR_DAC_BASE + I2C_ADR_DAC_OFFS,
   I2C_ADR_DAC_BASE + I2C_ADR_DAC_OFFS, I2C_ADR_DAC_BASE + I2C_ADR_DAC_OFFS};
/* DAC data buffer */
uint8_t bf_dac[DAC_COUNT];
/* DAC reference voltages in mV */
uint32_t ref_dac[DAC_COUNT] = { REF_DAC_1, REF_DAC_2, REF_DAC_3, REF_DAC_4 };
/* Universal string buffer */
char s[40];
/* The Top Level state */
state_t state;


/* List the current DAC configuration */
uint8_t dac_list(uint8_t index);
/* DAC setting */
uint8_t dac_set(void);
/* Autodetect DAC I2C address */
uint8_t i2c_detect_dac(uint8_t index);
/* Autodetect all DAC I2C addresses */
uint8_t i2c_detect_all_dacs(void);
/* Autodetect MUX I2C address */
static uint8_t i2c_detect_mux(void);
/* Update all DACs */
static void dac_update(void);


/* Setup function */
void setup(void)
{
  // serial port initialization
  Serial.begin(19200);
  Serial.setTimeout(10000);
  Serial.write('\n');

  // CCU emulation module initilalization
  ccu_i2c_init();

  state = S_LOAD_CFG;
  return;
}


/* Main loop function */
void loop(void)
{
  uint8_t i;
  int c;

  // The Top Level state machine
  switch (state)
  {
    case S_LOAD_CFG:  // load default configuration
#ifdef CFG_PRESET
      if (user_conf_read(bf_dac, DAC_COUNT) == CONF_RD_FAILED)
        memset(bf_dac, DAC_DEFAULT, DAC_COUNT);
#else
        memset(bf_dac, DAC_DEFAULT, DAC_COUNT);
#endif
      state = S_DETECT_MUX;
      break;

    case S_DETECT_MUX:  // autodetect MUX I2C address
#ifdef ADR_AUTO_MUX
      while ((i2c_adr_mux = i2c_detect_mux()) == 0)
        delay(500);
#else
      i2c_adr_mux = I2C_ADR_MUX_BASE + I2C_ADR_MUX_OFFS;
#endif
      state = S_DETECT_DAC;
      break;

    case S_DETECT_DAC:  // autodetect DAC I2C addresses
#ifdef ADR_AUTO_DAC
      while (i2c_detect_all_dacs() < 1)  // find at least one DAC
        delay(500);
#else
      memset(i2c_adr_dac, I2C_ADR_DAC_BASE + I2C_ADR_DAC_OFFS, DAC_COUNT);
#endif
      state = S_LIST;
      break;
      
    case S_DAC_UPDATE:  // update the tested system and ask for new command
      dac_update();
      Serial.print(F("Commands: (s)et, (w)rite cfg, (r)ead cfg, (l)ist?\n"));
      state = S_WAIT_CMD;
      break;

    case S_WAIT_CMD:  // next function selection
      c = Serial.read();
      switch (c)
      {
        case -1: break;
        case 's': state = S_SET; break;
        case 'w': state = S_WRITE_CFG; break;
        case 'r': state = S_READ_CFG; break;
        case 'l': state = S_LIST; break;
        default:
          if (c != '\n' && c != '\r') break;
      }
      break;

    case S_SET:  // set new value
      if ((i = dac_set()) != 0xFF) dac_list(i);
      state = S_DAC_UPDATE;
      break;

    case S_WRITE_CFG:  // write configuration
      user_conf_write(bf_dac, DAC_COUNT);
      user_conf_reset();
      state = S_WAIT_CMD;
      break;

    case S_READ_CFG:  // read configuration and update values
      if (user_conf_read(bf_dac, DAC_COUNT) == CONF_RD_FAILED)
        memset(bf_dac, DAC_DEFAULT, DAC_COUNT);
      user_conf_reset();
      state = S_LIST;
      break;

    case S_LIST:  // list and update actual values
      for (i = 0; i < 4; i++)
        dac_list(i);
      state = S_DAC_UPDATE;
      break;
  }
  return;
}


/* List the current DAC configuration */
uint8_t dac_list(uint8_t index)
{
  uint32_t vout;

  Serial.write("DAC no. ");
  Serial.print(index + 1, DEC);
  Serial.write(' adr: 0x');
  if (i2c_adr_dac[index] < 16) Serial.write('0');
  Serial.print(i2c_adr_dac[index], HEX);
  Serial.write(' ');
  Serial.write(" is set to 0x");
  if (bf_dac[index] < 16) Serial.write('0');
  Serial.print(bf_dac[index], HEX);
  Serial.write(" (");
  Serial.print(bf_dac[index], DEC);
  Serial.write(") ~ ");
  vout = ref_dac[index] * bf_dac[index] / 255;
  Serial.print(vout, DEC);
  Serial.write(" mV (");
  Serial.print(ref_dac[index], DEC);
  Serial.write(" mV ref)");
  Serial.write('\n');
  return;
}


/* DAC setting */
uint8_t dac_set(void)
{
  int c;
  uint8_t index;

  Serial.print(F("Which DAC? (1~Chan.1, 2~Chan.2, 3~Chan.3, 4~Chan.4)\n"));
  do {
    c = Serial.read();
    if (isalnum(c))
    {
      Serial.write(c);
      Serial.write('\n');
    } 
    if (c == '-') return(0xFF);
  } while (c != '1' && c !='2' && c !='3' && c !='4');
  index = c - '1';

  Serial.write("Previous value was ");
  Serial.print(bf_dac[index], DEC);
  Serial.write(", new one? (0-255)\n");
  Serial.read();
  while (!Serial.available())
    ;
  do {
    c = Serial.parseInt();
    Serial.print(c, DEC);
    Serial.write('\n');
    if (c == -1) return(0xFF);
  } while (c < 0 || c > 255);
  bf_dac[index] = c;

  return(index);
}


/* Autodetect DAC I2C address */
uint8_t i2c_detect_dac(uint8_t index)
{
  uint8_t adr;

  Serial.write("  |- Looking for NINO board DAC ch. ");
  Serial.print(index + 1, DEC);
  Serial.write(" at I2C address ");

  for (adr = I2C_ADR_DAC_BASE; adr <= I2C_ADR_DAC_BASE + I2C_ADR_DAC_MAX; adr++)
  {
    if (adr < 16) Serial.write('0');
    Serial.print(adr, HEX);
    if (ccu_i2c_write(adr, bf_dac[index])) {
      Serial.write(' ');
      delay(100);
      continue;
    } else {
      Serial.write(" - OK\n");
      return(adr);
    }
  }
  Serial.write(" - NO RESPONSE\n");
  return(0);
}

/* Autodetect all DAC I2C addresses */
uint8_t i2c_detect_all_dacs(void)
{
  uint8_t i, ret = 0;

  // test all DACs
  for (i = 0; i < 4; i++) {
    ccu_i2c_write(i2c_adr_mux, 1 << i);
    ccu_i2c_write(i2c_adr_mux, 1 << i);
    // find a DAC
    if ((i2c_adr_dac[i] = i2c_detect_dac(i)) != 0) ret++;
  }
  // disable last DAC
  ccu_i2c_write(i2c_adr_mux, 0x00);
  ccu_i2c_write(i2c_adr_mux, 0x00);

  return(ret);
}


/* Autodetect MUX I2C address */
static uint8_t i2c_detect_mux(void)
{
  uint8_t adr;

  Serial.write("Looking for NINO board mux at I2C address ");

  for (adr = I2C_ADR_MUX_BASE; adr <= I2C_ADR_MUX_BASE + I2C_ADR_MUX_MAX; adr++)
  {
    if (adr < 16) Serial.write('0');
    Serial.print(adr, HEX);
    if (ccu_i2c_write(adr, 0)) {
      Serial.write(' ');
      delay(100);
      continue;
    } else {
      Serial.write(" - OK\n");
      return(adr);
    }
  }
  Serial.write(" - NO RESPONSE\n");
  return(0);
}


/* Update all DACs */
static void dac_update(void)
{
  uint8_t i;

  // update all DACs
  for (i = 0; i < 4; i++) {
    ccu_i2c_write(i2c_adr_mux, 1 << i);
    ccu_i2c_write(i2c_adr_mux, 1 << i);
    ccu_i2c_write(i2c_adr_dac[i], bf_dac[i]);
  }
  // disable last DAC
  ccu_i2c_write(i2c_adr_mux, 0x00);
  ccu_i2c_write(i2c_adr_mux, 0x00);
  Serial.write("DACs updated\n");
  return;
}
