/* TOTEM Roman Pot FeedThrough and Nino Board Testing Device Arduino based
 * tot_cfg.h - configuration storage routines
 * Richard Linhart, University of West Bohemia in Pilsen
 * 2021-02 rlinhart@fel.zcu.cz
 */

#ifndef _TOT_CFG_H_
#define _TOT_CFG_H_


/* Configuration reading succeed */
#define CONF_RD_OK       0

/* Configuration reading failed */
#define CONF_RD_FAILED   1


/* User configuration - reset the read and write pointer */
extern void user_conf_reset(void);

/* User configuration - Write to the EEPROM */
extern uint8_t user_conf_write(uint8_t *buf, uint16_t len);

/* User configuration - Read from the EEPROM */
extern uint8_t user_conf_read(uint8_t *buf, uint16_t len);


#endif
